# phobos-web

Mise en place d'un pipeline CI/CD:

Dans cet exercice vous allez effectuer les actions suivantes:
- coder un serveur web très simple
- créer un projet GitLab pour gérer les sources
- mettre en place un cluster Kubernetes basé sur k3s
- intégrer ce cluster dans le projet GitLab
- mettre en place un pipeline d'integration / déploiement continu

Le but de l'ensemble de ces actions étant qu'une modification envoyée dans le projet GitLab
déclenche automatiquement le déployment de la nouvelle version du code sur le cluster
Kubernetes.


## app.py
Un serveur web simple ayant les caractéristiques suivantes:
- écoute sur le port 8000
- expose le endpoint / en GET
- retourne la chaine 'Hi!' pour chaque requète reçue
- Créez également un Dockerfile pour packager le serveur dans une image Docker


# Construction de l'image
Créez ensuite une image, nommée api, dans laquelle sera packagé le serveur mise en place
dans l'étape précédente:
```bash
$ docker build -t api .
```

## Test
Une fois l'image créée, lancez un container
```bash
$ docker run -ti -p 8000:8000 api
```

Puis vérifiez, depuis un autre terminal que le serveur fonctionne correctement:
```bash
$ curl http://localhost:8000
```


# Gestion du projet dans GitLab

## Création d'un repository
- Créez un compte sur GitLab et Push du code

## Mise en place d'un pipeline d'intégration continue
Créez un fichier .gitlab-ci.yml à la racine du projet :
```yaml
stages:
- package
push image docker:
image: docker:stable
stage: package
services:
- docker:18-dind
script:
- docker build -t $CI_REGISTRY_IMAGE:latest .
- docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
- docker push $CI_REGISTRY_IMAGE:latest
```
- Ces instructions définissent un stage nommé package contenant les commandes servant à
- créer une image Docker et à envoyer celle-ci dans le registry GitLab. Commitez l'ajout de ce
fichier et envoyez ces changements sur GitLab.

- Depuis le menu CI/CD de GitLab, vérifiez que la pipeline a été déclenchée.
- Une fois que cette pipeline est terminée, allez dans le menu Packages -> Container Registry et
- vérifiez que cette première image est maintenant présente dans le registry.


### Ajout de tests d'intégration
- Dans le fichier .gitlab-ci.yml , ajoutez une nouvelle entrée integration sous la clé stage.
- A la fin du fichier, ajoutez le step integration test suivant:
```yaml
integration test:
  image: docker:stable
  stage: integration
  services:
    - docker:18-dind
  script:
    - docker run -d --name myapp $CI_REGISTRY_IMAGE:latest
    - sleep 10s
    - TEST_RESULT=$(docker run --link myapp lucj/curl -s http://myapp:8000)
    - echo $TEST_RESULT
    - $([ "$TEST_RESULT" == "Hello World!" ])
```

- Ce step définit un test de l'image créée. 
- Il vérifie que la chaine "Hello World!" est retournée.


Envoyez ces modifications dans le repository GitLab:
```bash
$ git add .gitlab-ci.yml
$ git commit -m 'Add integration step'
$ git push origin master
```

- Depuis l'interface GitLab, vérifiez que la pipeline est déclenchée.
- Vous devriez voir que l'étape integration a terminé en erreur.
    - En cas d'erreur, regardez dans les logs du job, corrigez le code, commitez et pushez les changements. Vérifiez ensuite que le job passe correctement cette fois ci.


## Déploiement automatique
- Depuis le menu Operations > Environments, créez un environment test.
- Dans le fichier .gitlab-ci.yml ajoutez une nouvelle entrée nommée deploy sous la clé
stages:
```yaml
stages:
- package
- integration
- deploy
```

A la fin du fichier, ajoutez les instructions suivantes:
```yaml
deploy test:
  stage: deploy
  script:
    - echo "Deploy to test server"
  environment:
    name: test
```
Envoyez cette mise à jour sur GitLab puis vérifiez que la pipeline est déclenchée.

Une fois la pipeline terminée, vous pourrez voir qu'un nouveau déploiement est présent:
(Opération > Environnemnts).

Celui-ci ne fait pas grand chose car nous n'avons pas encore défini les étapes nécessaires
afin de déployer l'image qui a été créée. Dans la prochaine étape, vous allez déployer votre
serveur sur un cluster Kubernetes.


# Intégrez un cluster Kubernetes au repository GitLab
Les éléments suivants vous seront donnés:
- l'adresse IP d'une machine virtuelle créée sur un cloud provider
- une clé ssh pour vous y connecter

Lancez un shell sur cette machine virtuelle (en remplaçant le chemin d'accès de la clé privée
ainsi que l'adresse IP de votre machine): 
```bash
$ ssh -i PATH_TO_PRIVATE_KEY USERNAME@NODE_IP_ADDRESS
```

## Installation de K3s
Vous allez maintenant installer k3s.io, une distribution Kubernetes trèe light créée par Rancher.
```bash
$ curl -sfL https://get.k3s.io | sh -
```
L'installation ne devrait prendre que quelques dizaines de secondes. Vérifiez ensuite la liste
des nodes de votre cluster avec la commande suivante:
```bash
$ kubectl get node
```

Récupérez ensuite le fichier de configuration du cluster, celui-ci est présent dans
/etc/rancher/k3s/k3s.yaml:
```bash
$ scp -i PATH_TO_PRIVATE_KEY root@NODE_IP_ADDRESS:/etc/rancher/k3s/k3s.yaml
k3s.yaml-tmp
```

Dans ce fichier remplacez l'adresse IP local avec l'IP de la machine distante:
```bash
$ cat k3s.yaml-tmp | sed 's/127.0.0.1/NODE_IP_ADDRESS/' > k3s.yaml
```

Positionnez ensuite la variable d'environnement KUBECONFIG de façon à ce qu'elle référence
le fichier k3s.yaml :
```bash
$ export KUBECONFIG=$PWD/k3s.yaml
```

Depuis votre machine locale, vous devriez maintenant pouvoir communiquer avec le cluster
Kubernetes:
```bash
$ kubectl get nodes
$ kubectl --insecure-skip-tls-verify get node (Bypass du certificat)
$ systemctl stop k3s
$ sudo vi /etc/systemd/system/k3s.service
$ k3s server --tls-san PUBLIC_IP_CLOUD
```

## Intégration du cluster avec votre repository GitLab
Depuis le menu Operations > Kubernetes de l'interface GitLab, selectionnez Add existing
cluster.

Afin d'intégrer, dans le projet GitLab, le cluster k3s que vous avez mis en place
précédemment, vous allez suivre les étapes ci-dessous:

- installez jq sur votre machine locale. jq est un utilitaire très pratique (et très utilisé) pour
manipuler les structures json, il peut être installé depuis jq.
- ensuite, depuis le terminal dans lequel vous avez défini la variable d'environnement
KUBECONFIG, executez la commande suivante:
```bash
$ curl -sSL https://files.techwhale.io/kubeconfig.sh | bash
```

Vous obtiendrez les informations nécessaires pour l'intégration de votre cluster Kubernetes
dans votre projet GitLab:
- le nom du cluster (vous êtes libre de changer celui-ci si vous le souhaitez)
- l'URL de l'API Server
- l'authorité de certification du cluster
- un token d'authentification

entrez ces informations dans les champs qui correspondent et assurez vous d'avoir
déselectionnez la checkbox GitLab-managed cluster

Une fois le formulaire validé, votre projet GitLab sera pourra communiquer avec votre cluster
Kubernetes.

https://gitlab.com/help/user/project/clusters/add_remove_clusters.md#add-existing-cluster


qwiklabs



## Ajout des fichiers de spécifications
A la racine de votre projet, copiez le contenu suivant dans le fichier deploy.yml .

- Note: remplacez GITLAB_USER avec votre nom d'utilisateur GitLab et REPOSITORY
avec le nom de votre projet sur GitLab
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: www
  labels:
    app: www
spec:
  selector:
    matchLabels:
      app: www
  replicas: 2
  template:
    metadata:
      labels:
        app: www
    spec:
      containers:
      - name: www
        image: registry.gitlab.com/GITLAB_USER/GITLAB_PROJECT:latest
        imagePullPolicy: Always
```

Au même endroit, placez le contenu suivant dans un fichier service.yml .
```yaml
apiVersion: v1
kind: Service
metadata:
  name: www
spec:
  type: NodePort
  ports:
    - name: www
      nodePort: 31000
      port: 80
      targetPort: 8000
      protocol: TCP
  selector:
    app: www
```

Lancez les commandes suivantes afin de déployer l'application dans le cluster:
```bash
$ kubectl apply -f deploy.yml
$ kubectl apply -f service.yml
```

Assurez vous que les ressources ont été créées correctement:
```bash
$ kubectl get deploy,pod,svc
```

En utilisant l'adresse IP de la VM dans laquelle tourne kubernetes, vérifiez que le serveur web
est disponible sur le port 31000.

Note: l'adresse IP peut également être obtenue avec la commande suivante: $ kubectl get
node -o wide
```bash
$ curl http://NODE_IP_ADDRESS:31000
Hello World!
```


# Mise en place du deployment automatique
Dans le fichier .gitlab-ci.yml modifiez l'étape "deploy test" avec les instructions suivantes:
```yaml
deploy test:
  stage: deploy
  environment: staging
  image: lucj/kubectl:1.16.2
  script:
    - kubectl config set-cluster my-cluster --server=${KUBE_URL} --certificateauthority="${KUBE_CA_PEM_FILE}"
    - kubectl config set-credentials admin --token=${KUBE_TOKEN}
    - kubectl config set-context my-context --cluster=my-cluster --user=admin --
namespace default
    - kubectl config use-context my-context
    - kubectl rollout restart deploy/www
  only:
    kubernetes: active
```
Les instructions définies sous la clé script permettent de:
- créer un context (au sens Kubernetes) à l'aide des variables d'environnement
automatiquement créées lors de l'intégration du cluster dans le projet GitLab
- configurer le binaire kubectl à l'aide de ce context
- mettre à jour le Deployment

Note: l'image utilisée (lucj/kubectl:1.16.2) contient seulement le binaire kubectl dont nous
avons besoin dans le pipeline d'intégration continue afin de communiquer avec le cluster

Pour vérifiez qu'un changement dans le code du serveur déclenche une mise à jour de
l'application, effectuez les modifications suivantes:

- changez la chaine de caractères retournée par le serveur en "Hello!"
- modifiez le test présent dans .gitlab-ci.yml

Publiez les changements dans GitLab. Puis, une fois que la pipeline est terminée, testez une
nouvelle fois l'application.
```bash
$ curl http://NODE_IP_ADDRESS:31000
Hello!
```